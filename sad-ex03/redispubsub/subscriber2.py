import redis
import json
import time
import threading
r = redis.Redis()
ps = r.pubsub() 
ps.subscribe({"message"})
def main_loop():
    while True:
        msg = ps.get_message(ignore_subscribe_messages=True)
        if msg:
            print(msg["data"].decode())
        time.sleep(0.001) 
threading.Thread(None,main_loop).start()