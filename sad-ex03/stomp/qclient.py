import time 
import stomp

class MyListener(stomp.ConnectionListener):
    def on_error(self, headers, message):
        print('received an error "%s"' % message)
    def on_message(self, headers, message):
        print('received a message "%s"' % message)

conn = stomp.Connection()
conn.set_listener('', MyListener())
conn.connect(wait=True)

conn.subscribe(destination='/queue/results', id=1, ack='auto')

while True:
    time.sleep(0.01)
conn.disconnect()