import redis
import json

r = redis.Redis() 

user = {
    "firstname":input("Firstname: "),
    "lastname":input("Lastname: ")
}
r.set("user",json.dumps(user)) 

user = json.loads(r.get("user")) 
print(
    "User from redis:",
    user["firstname"],
    user["lastname"]
)

