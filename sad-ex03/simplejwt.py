import jwt
import time 
key = "secret_key"
data = {
    "role":"admin",
    "exp":time.time()+30
} 
token = jwt.encode(data,key,'HS256')  
print("Generated token:")
print(token) 
print("Extracted data:")
edata = jwt.decode(token,key,'HS256')
print(edata)