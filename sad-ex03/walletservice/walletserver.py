import logging
import secrets
logging.basicConfig(level=logging.DEBUG)
logging.getLogger("sqlalchemy").setLevel(logging.DEBUG)
from spyne import Iterable 
from spyne.protocol.soap import Soap12
from spyne.server.wsgi import WsgiApplication 
from spyne import Application, rpc, ServiceBase, Unicode, Integer32, Double, TTableModel, ServiceBase, Array 
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker 

TableModel = TTableModel()
db = create_engine("mysql+mysqlconnector://root:@localhost/wallet")
Session = sessionmaker(bind=db)
TableModel.Attributes.sqla_metadata.bind = db

class User(TableModel):
    __tablename__ = 'user'
    id = Integer32(primary_key=True)
    username    = Unicode(256, nullable=False)
    password    = Unicode(256)
    token       = Unicode(256)
    balance     = Double

class WalletService(ServiceBase):
    @rpc(Unicode, Unicode, _returns=Unicode)
    def login(self, username, password): 
        session = Session()
        token = ""
        try:
            token = ""
            usr = session.query(User).filter_by(username=username,password=password).first()
            if usr:
                token = secrets.token_hex(16)
                usr.token = token
                session.commit()     
        except:
            print("Error")
        finally: 
            session.close()
        return token
    @rpc(Unicode, _returns=Double)
    def get_balance(self, token): 
        session = Session() 
        try: 
            usr = session.query(User).filter_by(token=token).first()
            if usr:
                return usr.balance     
        except:
            print("Error")
        finally: 
            session.close()
        return token
    @rpc(Unicode, Double, _returns=Double)
    def deposit(self, token, amount): 
        session = Session() 
        try: 
            usr = session.query(User).filter_by(token=token).first()
            if usr:
                usr.balance += amount
                session.commit()
                session.refresh(usr)
                return usr.balance     
        except:
            print("Error")
        finally: 
            session.close()
        return -1
    @rpc(Unicode, Double, _returns=Double)
    def withdraw(self, token, amount): 
        session = Session() 
        try: 
            usr = session.query(User).filter_by(token=token).first()
            if usr:
                if usr.balance - amount >= 0:
                    usr.balance -= amount
                    session.commit()
                    session.refresh(usr) 
            return usr.balance     
        except:
            print("Error")
        finally: 
            session.close()
        return -1
        
        
        

application = Application([WalletService],
    tns='com.sadex03.wallet',
    in_protocol=Soap12(),
    out_protocol=Soap12()
)
if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()