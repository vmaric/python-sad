from zeep import Client,Settings
from zeep.cache import SqliteCache
from zeep.transports import Transport
cache = SqliteCache(path='sqlite.db', timeout=60)
transport = Transport(cache=cache)

settings = Settings(strict=False, xml_huge_tree=True) 
cl = Client("http://127.0.0.1:8000/?wsdl",settings=settings,transport=transport)     

user_token = cl.service.login("dovla","123") 
if user_token:
    print("Logged in with token:",user_token)
    print("Current balance:",cl.service.get_balance(user_token))
    print("Depositing 10:",cl.service.deposit(user_token,10))
    print("Current balance:",cl.service.get_balance(user_token))
    print("Withdrawing 5.2:",cl.service.withdraw(user_token,5.2))
    print("Current balance:",cl.service.get_balance(user_token))
    