create table user (
    id int primary key auto_increment, 
    username varchar(50), 
    password varchar(50), 
    balance decimal(9,2),
    token varchar(32)
) engine = innodb;