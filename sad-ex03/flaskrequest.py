from flask import Flask, request

app = Flask(__name__)

@app.route("/api/<resource>")
def req(resource):
    if request.method.lower() == "get":
        return f"Method is GET for resource {resource}"
    elif request.method.lower() == "post":
        return f"Method is POST resource {resource}"
    elif request.method.lower() == "put":
        return f"Method is PUT resource {resource}"
    elif request.method.lower() == "delete":
        return f"Method is DELETE resource {resource}"

app.run()