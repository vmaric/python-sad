import asyncio
import websockets
import time,threading 
users = {}   
async def socket_connect(wsocket, path):
    users[wsocket] = wsocket 
    try:
        while True:
            newmsg = await wsocket.recv()
            for u in users:
                await users[u].send(newmsg) 
    except:
        del users[wsocket]

async def clientconnect(r,w):
    await r.read(2048)
    code = ''' 
    <textarea id='messages'></textarea><br>
    <input type='text' id='message' />
    <script>
    var messages = document.getElementById('messages');
    document.getElementById('message').onkeyup = function(evt){
        if(evt.keyCode==13){
            ws.send(this.value)
            this.value=''
        }
    }
    var ws = new WebSocket("ws://localhost:8765");
    ws.onmessage = function(data){ 
        messages.innerHTML+=data.data+'\\n'
        messages.scrollTop = messages.scrollHeight;
    }
    </script>
    '''
    w.write(f"HTTP/1.1 200 Ok\r\nContent-type:text/html\r\n\r\n{code}".encode())
    w.close() 
start_server = websockets.serve(socket_connect, "localhost", 8765) 
webserver = asyncio.start_server(clientconnect,"localhost",8000)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_until_complete(webserver)
asyncio.get_event_loop().run_forever()