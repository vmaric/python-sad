import xml.sax as sax

class StocksHandler(sax.ContentHandler):
    def __init__(self):
        self.element_name = ""
        self.items_started = False
        self.enames = {"title":"Title","link":"Link","pubDate":"Publishing date","description":"Descripition"}
    def startElement(self, name, attrs):
        self.element_name = name
        if name == "item":
            self.items_started = True
        if self.items_started and name in ["title","link","pubDate","description"]:
            print(self.enames[name],": ",end="")
    def endElement(self, name): 
        self.element_name = ""
        if self.items_started and name in ["title","link","pubDate","description"]:
            print()
    def characters(self, content):
        if not self.items_started:
            return
        if self.items_started and self.element_name in ["title","link","pubDate","description"]:
            print(content,end="")

parser = sax.make_parser()
parser.setFeature(sax.handler.feature_namespaces, 0)
parser.setContentHandler(StocksHandler())
parser.parse("http://rss.news.yahoo.com/rss/stocks")

