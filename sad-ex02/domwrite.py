import xml.dom.minidom as dom


xml = dom.parse("books.xml") 
root = xml.documentElement

book = xml.createElement("book")
author = xml.createElement("author")
title = xml.createElement("title")
author_content = xml.createTextNode("Isaac Asimov")
title_content = xml.createTextNode("The Caves of Steel") 

author.appendChild(author_content)
title.appendChild(title_content)
 
book.setAttribute("id","3")
book.setAttribute("isbn","111213")

book.appendChild(author)
book.appendChild(title)

root.appendChild(book)

print(xml.toxml())


