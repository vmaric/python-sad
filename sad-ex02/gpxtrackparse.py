import xml.dom.minidom as xmldom
from urllib.request import Request, urlopen

xml = xmldom.parse("yosemite.xml")

locations = xml.getElementsByTagName("wpt")

for location in locations:
    name = location.getElementsByTagName("name")[0].firstChild.nodeValue
    lat = location.attributes["lat"].nodeValue
    lon = location.attributes["lon"].nodeValue
    print("########### LOCATION ###########")
    print("Latitude:",lat)
    print("Longitude:",lon)
    print("Name:",name)