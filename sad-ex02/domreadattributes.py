import xml.dom.minidom as dom


xml = dom.parse("books.xml") 
root = xml.documentElement
books = root.getElementsByTagName("book") 
for book in books:
    bid = book.attributes["id"]
    isbn = book.attributes["isbn"]
    print("#######",bid.nodeValue,"/",isbn.nodeValue,"######")
    print("Title:",book.getElementsByTagName("title")[0].firstChild.nodeValue)
    print("Author:",book.getElementsByTagName("author")[0].firstChild.nodeValue)


