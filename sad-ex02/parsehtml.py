import xml.dom.minidom as xmldom
from urllib.request import Request, urlopen

xml = xmldom.parse("page.html")
links = xml.getElementsByTagName("a") 
for link in links:
    url = link.attributes["href"].nodeValue
    req = Request(url,headers={'User-Agent': 'Mozilla/5.0'})
    res = urlopen(req)
    print("Writing:",url)
    f = open(url.split("://")[1].replace("/","_"),"wb")
    f.write(res.read()) 